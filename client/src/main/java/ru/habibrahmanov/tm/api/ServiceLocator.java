package ru.habibrahmanov.tm.api;

import ru.habibrahmanov.tm.command.AbstractCommand;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public interface ServiceLocator {

    TreeMap<String, AbstractCommand> sortCommands();

    Map<String, AbstractCommand> getCommands();

    Scanner getScanner();

    IProjectEndpoint getProjectEndpoint();

    ISessionEndpoint getSessionEndpoint();

    ITaskEndpoint getTaskEndpoint();

    IUserEndpoint getUserEndpoint();

    SessionDto getCurrentSession();

    IDomainEndpoint getDomainEndpoint();

    void setCurrentSession(SessionDto currentSession);
}

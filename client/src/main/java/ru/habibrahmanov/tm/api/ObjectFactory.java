
package ru.habibrahmanov.tm.api;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.habibrahmanov.tm.api package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _IncorrectValueException_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "IncorrectValueException");
    private final static QName _SessionIsNotValidException_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "SessionIsNotValidException");
    private final static QName _Close_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "close");
    private final static QName _CloseResponse_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "closeResponse");
    private final static QName _Open_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "open");
    private final static QName _OpenResponse_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "openResponse");
    private final static QName _Validate_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "validate");
    private final static QName _ValidateResponse_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "validateResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.habibrahmanov.tm.api
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IncorrectValueException }
     * 
     */
    public IncorrectValueException createIncorrectValueException() {
        return new IncorrectValueException();
    }

    /**
     * Create an instance of {@link SessionIsNotValidException }
     * 
     */
    public SessionIsNotValidException createSessionIsNotValidException() {
        return new SessionIsNotValidException();
    }

    /**
     * Create an instance of {@link Close }
     * 
     */
    public Close createClose() {
        return new Close();
    }

    /**
     * Create an instance of {@link CloseResponse }
     * 
     */
    public CloseResponse createCloseResponse() {
        return new CloseResponse();
    }

    /**
     * Create an instance of {@link Open }
     * 
     */
    public Open createOpen() {
        return new Open();
    }

    /**
     * Create an instance of {@link OpenResponse }
     * 
     */
    public OpenResponse createOpenResponse() {
        return new OpenResponse();
    }

    /**
     * Create an instance of {@link Validate }
     * 
     */
    public Validate createValidate() {
        return new Validate();
    }

    /**
     * Create an instance of {@link ValidateResponse }
     * 
     */
    public ValidateResponse createValidateResponse() {
        return new ValidateResponse();
    }

    /**
     * Create an instance of {@link SessionDto }
     * 
     */
    public SessionDto createSessionDto() {
        return new SessionDto();
    }

    /**
     * Create an instance of {@link AbstractDto }
     * 
     */
    public AbstractDto createAbstractDto() {
        return new AbstractDto();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IncorrectValueException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IncorrectValueException }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "IncorrectValueException")
    public JAXBElement<IncorrectValueException> createIncorrectValueException(IncorrectValueException value) {
        return new JAXBElement<IncorrectValueException>(_IncorrectValueException_QNAME, IncorrectValueException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SessionIsNotValidException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SessionIsNotValidException }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "SessionIsNotValidException")
    public JAXBElement<SessionIsNotValidException> createSessionIsNotValidException(SessionIsNotValidException value) {
        return new JAXBElement<SessionIsNotValidException>(_SessionIsNotValidException_QNAME, SessionIsNotValidException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Close }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Close }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "close")
    public JAXBElement<Close> createClose(Close value) {
        return new JAXBElement<Close>(_Close_QNAME, Close.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CloseResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "closeResponse")
    public JAXBElement<CloseResponse> createCloseResponse(CloseResponse value) {
        return new JAXBElement<CloseResponse>(_CloseResponse_QNAME, CloseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Open }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Open }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "open")
    public JAXBElement<Open> createOpen(Open value) {
        return new JAXBElement<Open>(_Open_QNAME, Open.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link OpenResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "openResponse")
    public JAXBElement<OpenResponse> createOpenResponse(OpenResponse value) {
        return new JAXBElement<OpenResponse>(_OpenResponse_QNAME, OpenResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Validate }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Validate }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "validate")
    public JAXBElement<Validate> createValidate(Validate value) {
        return new JAXBElement<Validate>(_Validate_QNAME, Validate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ValidateResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "validateResponse")
    public JAXBElement<ValidateResponse> createValidateResponse(ValidateResponse value) {
        return new JAXBElement<ValidateResponse>(_ValidateResponse_QNAME, ValidateResponse.class, null, value);
    }

}

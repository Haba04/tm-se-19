package ru.habibrahmanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.Project;
import ru.habibrahmanov.tm.api.Task;
import ru.habibrahmanov.tm.command.AbstractCommand;

public final class TaskMergeCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-merge";
    }

    @Override
    public String getDescription() {
        return "update if task is already exist, else create new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK MERGE]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER TASK ID:");
        @Nullable final String id = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER TASK NAME:");
        @Nullable final String name = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        @Nullable final String description = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER TASK DATE BEGIN:");
        @Nullable final String dateBegin = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER TASK DATE END:");
        @Nullable final String dateEnd = serviceLocator.getScanner().nextLine();
        @Nullable final String userId = serviceLocator.getCurrentSession().getUserId();
        System.out.println("ENTER TASK STATUS:");
        @Nullable final String status = serviceLocator.getScanner().nextLine();
        @NotNull final Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
//        task.setDateBegin(dateFormat.parse(dateBegin));
//        task.setDateEnd(dateFormat.parse(dateEnd));
        serviceLocator.getTaskEndpoint().mergeTask(serviceLocator.getCurrentSession(), task);
        System.out.println("TASK MERGE SUCCESSFULLY");
    }
}
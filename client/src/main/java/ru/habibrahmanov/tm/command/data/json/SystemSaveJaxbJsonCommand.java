package ru.habibrahmanov.tm.command.data.json;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class SystemSaveJaxbJsonCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "save-jaxb-json";
    }

    @Override
    public String getDescription() {
        return "saving the subject area using externalization and jaxb technology in json transport format";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getDomainEndpoint().saveJaxbJson(serviceLocator.getCurrentSession());
        System.out.println("OK");
    }
}

package ru.habibrahmanov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.command.AbstractCommand;

public final class ProjectRemoveAllCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-remove-all";
    }

    @Override
    public String getDescription() {
        return "remove all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CLEAR]");
        serviceLocator.getProjectEndpoint().removeAllProject(serviceLocator.getCurrentSession());
        System.out.println("DELETE ALL PROJECTS");
        serviceLocator.getUserEndpoint();
    }
}

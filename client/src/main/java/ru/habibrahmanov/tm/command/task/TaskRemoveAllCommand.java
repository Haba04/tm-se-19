package ru.habibrahmanov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.command.AbstractCommand;

public final class TaskRemoveAllCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-remove-all";
    }

    @Override
    public String getDescription() {
        return "remove all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = serviceLocator.getScanner().nextLine();
        serviceLocator.getTaskEndpoint().removeAllTask(serviceLocator.getCurrentSession(), projectId);
        System.out.println("TASK REMOVED SUCCESSFULLY");
    }
}

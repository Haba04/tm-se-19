package ru.habibrahmanov.tm.command.data.json;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class SystemSaveJacksonJsonCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "save-jackson-json";
    }

    @Override
    public String getDescription() {
        return "saving the subject area using externalization and jackson technology in json transport format";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getDomainEndpoint().saveJacksonJson(serviceLocator.getCurrentSession());
        System.out.println("OK");
    }
}

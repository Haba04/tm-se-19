package ru.habibrahmanov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.command.AbstractCommand;

public final class ProjectUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-update";
    }

    @Override
    public String getDescription() {
        return "update project by id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER PROJECT ID");
        @Nullable final String projectId = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER NAME TO CHANGE");
        @Nullable final String name = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER STATUS (planned / inprogress / ready):");
        @Nullable final String status = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER DATE BEGIN:");
        @Nullable final String dateBegin = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER DATE END:");
        @Nullable final String dateEnd = serviceLocator.getScanner().nextLine();
        serviceLocator.getProjectEndpoint().updateProject(serviceLocator.getCurrentSession(), projectId, name, description, status, dateBegin, dateEnd);
        System.out.println("PROJECT EDIT SUCCESSFULLY");
    }
}

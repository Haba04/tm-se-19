package ru.habibrahmanov.tm.command.data.xml;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class SystemLoadJacksonXmlCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "load-jackson-xml";
    }

    @Override
    public String getDescription() {
        return "loading the subject area using externalization and jackson technology in xml transport format";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getDomainEndpoint().loadJacksonXml(serviceLocator.getCurrentSession());
        System.out.println("OK");
    }
}

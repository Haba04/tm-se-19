package ru.habibrahmanov.tm.command.user;

import ru.habibrahmanov.tm.command.AbstractCommand;

public final class UserViewProfileCommand extends AbstractCommand {
    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[VIEW PROFILE]");
        serviceLocator.getUserEndpoint().viewProfile(serviceLocator.getCurrentSession());
        System.out.println("your login: " + serviceLocator.getUserEndpoint().viewProfile(serviceLocator.getCurrentSession()).getLogin());
        System.out.println("your password: " + serviceLocator.getUserEndpoint().viewProfile(serviceLocator.getCurrentSession()).getPassword());
    }
}

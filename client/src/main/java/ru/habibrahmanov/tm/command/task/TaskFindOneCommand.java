package ru.habibrahmanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.TaskDto;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.api.Task;

public final class TaskFindOneCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-find";
    }

    @Override
    public String getDescription() {
        return "find task by id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        @Nullable final String taskId = serviceLocator.getScanner().nextLine();
        @NotNull final TaskDto task = serviceLocator.getTaskEndpoint().findOneTask(serviceLocator.getCurrentSession(), taskId);
        System.out.println(task);
        System.out.println();
    }
}

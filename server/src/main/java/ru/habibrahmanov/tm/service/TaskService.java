package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.habibrahmanov.tm.api.ITaskService;
import ru.habibrahmanov.tm.comporator.TaskDateBeginComparator;
import ru.habibrahmanov.tm.comporator.TaskDateEndComparator;
import ru.habibrahmanov.tm.dto.TaskDto;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.enumeration.Status;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.repository.IProjectRepository;
import ru.habibrahmanov.tm.repository.ITaskRepository;
import ru.habibrahmanov.tm.repository.IUserRepository;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class TaskService extends AbstractService implements ITaskService {

    @Autowired
    private ITaskRepository taskRepository;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IProjectRepository projectRepository;

    @Override
    public void persist(@Nullable final Task task) throws IncorrectValueException {
        if (task == null) throw new IncorrectValueException();
        taskRepository.save(task);
    }

    @Override
    public void insert(
            @Nullable final String projectId, @Nullable final String userId, @Nullable final String name,
            @Nullable final String description, @Nullable final String dateBegin, @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException {
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        @NotNull final User user = userRepository.getOne(userId);
        @NotNull final Project project = projectRepository.getOne(projectId);
        taskRepository.save(new Task(name, description, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd), project, user));
    }


    @NotNull
    @Override
    public TaskDto findOne(@Nullable final String taskId, @Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        if (taskId == null || taskId.isEmpty()) throw new IncorrectValueException();
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final Task task = taskRepository.findOneByUserIdAndId(taskId, userId);
        return convertEntityToDto(task);
    }

    @NotNull
    @Override
    public List<TaskDto> findAll(@Nullable final String userId) throws ListIsEmptyExeption, IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (taskRepository.findAllByUserId(userId).isEmpty()) throw new ListIsEmptyExeption();
        @Nullable final List<Task> taskList = taskRepository.findAllByUserId(userId);
        @Nullable final List<TaskDto> taskDtoList = new ArrayList<>();
        for (Task task : taskList) {
            taskDtoList.add(convertEntityToDto(task));
        }
        return taskDtoList;
    }

    @NotNull
    @Override
    public List<TaskDto> findAll() throws ListIsEmptyExeption {
        if (taskRepository.findAll().isEmpty()) throw new ListIsEmptyExeption();
        @Nullable final List<Task> taskList = taskRepository.findAll();
        @Nullable final List<TaskDto> taskDtoList = new ArrayList<>();
        for (Task task : taskList) {
            taskDtoList.add(convertEntityToDto(task));
        }
        return taskDtoList;
    }

    @Transactional
    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskId) throws IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (taskId == null || taskId.isEmpty()) throw new IncorrectValueException();
        taskRepository.deleteOneByUserIdAndId(userId, taskId);
    }

    @Transactional
    @Override
    public void removeAll(@Nullable final String userId, @Nullable final String projectId) throws IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        taskRepository.deleteAllByUserIdAndProjectId(userId, projectId);
    }

    @Transactional
    @Override
    public void update(@Nullable final String projectId, @Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description,
                       @Nullable final String status, @Nullable final String dateBegin, @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (id == null || id.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        switch (status) {
            case ("inprogress") :
                taskRepository.update(userId, id, name, description, Status.INPROGRESS, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd));

                break;
            case ("planned") :
                taskRepository.update(userId, id, name, description, Status.PLANNED, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd));

                break;
            case ("ready") :
                taskRepository.update(userId, id, name, description, Status.READY, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd));
                break;
        }
    }

    @Override
    public void merge(
            @NotNull final Task task
    ) throws IncorrectValueException, ParseException {
        if (taskRepository.findOneByUserIdAndId(task.getUser().getId(), task.getId()) == null) {
            persist(task);
        } else
            update(task.getProject().getId(), task.getUser().getId(), task.getId(), task.getName(), task.getDescription(), task.getStatus().displayName(),
                    dateFormat.format(task.getDateBegin()), dateFormat.format(task.getDateEnd()));
    }

    @NotNull
    @Override
    public List<TaskDto> searchByString(
            @Nullable final String userId, @Nullable final String string
    ) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (string == null || string.isEmpty()) throw new IncorrectValueException();
        if (taskRepository.searchByString(userId, string).isEmpty()) throw new ListIsEmptyExeption();
        @Nullable final List<Task> taskList = taskRepository.searchByString(userId, string);
        @Nullable final List<TaskDto> taskDtoList = new ArrayList<>();
        for (Task task : taskList) {
            taskDtoList.add(convertEntityToDto(task));
        }
        return taskDtoList;
    }

    @NotNull
    @Override
    public List<TaskDto> findAllSortedByDateBegin(@Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final List<TaskDto> taskList = findAll(userId);
        Collections.sort(taskList, new TaskDateBeginComparator());
        return taskList;
    }

    @NotNull
    @Override
    public List<TaskDto> findAllSortedByDateEnd(@Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final List<TaskDto> taskList = findAll(userId);
        Collections.sort(taskList, new TaskDateEndComparator());
        return taskList;
    }

    @NotNull
    @Override
    public List<TaskDto> findAllSortedByStatus(@Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final List<TaskDto> taskList = findAll(userId);
        Collections.sort(taskList);
        return taskList;
    }

    @Override
    public TaskDto convertEntityToDto(Task task) {
        TaskDto taskDto = new TaskDto();
        taskDto.setId(task.getId());
        taskDto.setName(task.getName());
        taskDto.setDescription(task.getDescription());
        taskDto.setStatus(task.getStatus());
        taskDto.setDateBegin(task.getDateBegin());
        taskDto.setDateEnd(task.getDateEnd());
        taskDto.setUserId(task.getUser().getId());
        return taskDto;
    }

    @Override
    public Task convertDtoToEntity(TaskDto taskDto) {
        Task task = new Task();
        task.setId(taskDto.getId());
        task.setName(taskDto.getName());
        task.setDescription(taskDto.getDescription());
        task.setStatus(taskDto.getStatus());
        task.setDateBegin(taskDto.getDateBegin());
        task.setDateEnd(taskDto.getDateEnd());
        return task;
    }
}

package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.NotNull;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyService {
    @NotNull private final Properties properties = new Properties();
    @NotNull private final String PATH_TO_PROPERTIES = "server/src/main/resources/app.properties";

    public PropertyService() {
        try {
            FileInputStream fileInputStream = new FileInputStream(PATH_TO_PROPERTIES);
            properties.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getUrl() {
        return properties.getProperty("url");
    }

    public String getDbType() {
        return properties.getProperty("dbType");
    }

    public String getHost() {
        return properties.getProperty("host");
    }

    public String getPort() {
        return properties.getProperty("port");
    }

    public String getDriver() {
        return properties.getProperty("driver");
    }

    public String getDbHost() {
        return properties.getProperty("dbHost");
    }

    public String getDbPort() {
        return properties.getProperty("dbPort");
    }

    public String getDbName() {
        return properties.getProperty("dbName");
    }

    public String getDbLogin() {
        return properties.getProperty("dbLogin");
    }

    public String getDbPassword() {
        return properties.getProperty("dbPassword");
    }

    public String getSalt() {
        return properties.getProperty("salt");
    }

    public String getCycle() {
        return properties.getProperty("cycle");
    }
}

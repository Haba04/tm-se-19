package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.habibrahmanov.tm.api.ISessionService;
import ru.habibrahmanov.tm.dto.SessionDto;
import ru.habibrahmanov.tm.entity.Session;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.SessionIsNotValidException;
import ru.habibrahmanov.tm.repository.ISessionRepository;
import ru.habibrahmanov.tm.repository.IUserRepository;
import ru.habibrahmanov.tm.util.HashUtil;
import ru.habibrahmanov.tm.util.SignatureUtil;
import javax.persistence.EntityManager;

@Service
public class SessionService implements ISessionService {

    @Autowired
    private ISessionRepository sessionRepository;

    @Autowired
    private IUserRepository userRepository;

    @Override
    public void validate(@Nullable final SessionDto session) throws SessionIsNotValidException {
        if (session == null) throw new SessionIsNotValidException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new SessionIsNotValidException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new SessionIsNotValidException();
        @Nullable final String currentSignature = session.getSignature();
        session.setSignature(null);
        @Nullable final String newSignature = SignatureUtil.sign(session, "qweasd", 7);
        if (!currentSignature.equals(newSignature)) throw new SessionIsNotValidException();
        @Nullable final Long timeStamp = session.getTimestamp();
        @Nullable final Long currentTimeStamp = System.currentTimeMillis();
        if (currentTimeStamp - timeStamp > 1800000) throw new SessionIsNotValidException("Session is time out");
    }

    @Override
    public SessionDto open(@Nullable final String login, @Nullable final String password) throws IncorrectValueException {
        if (login == null || login.isEmpty()) throw new IllegalArgumentException();
        if (password == null || password.isEmpty()) throw new IllegalArgumentException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) throw new IncorrectValueException("USER DON'T EXIST");
        @Nullable final String currentPassword = HashUtil.md5(password);
        if (currentPassword == null || currentPassword.isEmpty())
            throw new IncorrectValueException("WRONG PASSWORD");
        if (!user.getPassword().equals(currentPassword)) throw new IncorrectValueException("WRONG PASSWORD");
        @Nullable final SessionDto sessionDto = new SessionDto();
        sessionDto.setUserId(user.getId());
        @Nullable final String signature = SignatureUtil.sign(sessionDto, "qweasd", 7);
        sessionDto.setSignature(signature);
        @Nullable final Session session = convertDtoToEntity(sessionDto);
        sessionRepository.save(session);
        return sessionDto;
    }

    @Override
    public void close(@Nullable final SessionDto session) throws SessionIsNotValidException {
        if (session == null) throw new SessionIsNotValidException();
        sessionRepository.delete(convertDtoToEntity(session));
    }

    @Override
    public Session convertDtoToEntity(SessionDto sessionDto) {
        Session session = new Session();
        session.setId(sessionDto.getId());
        session.setUser(userRepository.getOne(sessionDto.getUserId()));
        session.setTimestamp(sessionDto.getTimestamp());
        session.setSignature(sessionDto.getSignature());
        return session;
    }
}

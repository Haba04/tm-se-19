package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.habibrahmanov.tm.api.IProjectService;
import ru.habibrahmanov.tm.dto.ProjectDto;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.enumeration.Status;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.repository.IProjectRepository;
import ru.habibrahmanov.tm.repository.ITaskRepository;
import ru.habibrahmanov.tm.repository.IUserRepository;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectService extends AbstractService implements IProjectService {

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private IUserRepository userRepository;

    @Override
    public void insert(
            @Nullable final String name, @Nullable final String description, @Nullable final String dateBegin,
            @Nullable final String dateEnd, @Nullable final String userId
    ) throws IncorrectValueException, ParseException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        @Nullable final User user = userRepository.getOne(userId);
        projectRepository.save(new Project(name, description, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd), user));
    }

    @NotNull
    public ProjectDto findOne(@Nullable final String userId, @Nullable final String projectId) throws IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        @Nullable final Project project = projectRepository.findByUserAndId(userId, projectId);
        return convertEntityToDto(project);
    }

    @Override
    public void persist(@Nullable final Project project) throws IncorrectValueException {
        if (project == null) throw new IncorrectValueException();
        projectRepository.save(project);
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@Nullable final String userId) throws ListIsEmptyExeption, IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectRepository.findAllByUserId(userId).isEmpty()) throw new ListIsEmptyExeption();
        @Nullable final List<Project> projectList = projectRepository.findAllByUserId(userId);
        @Nullable final List<ProjectDto> projectDtoList = new ArrayList<>();
        for (Project project : projectList) {
            projectDtoList.add(convertEntityToDto(project));
        }
        return projectDtoList;
    }

    @NotNull
    @Override
    public List<Project> findAll() throws ListIsEmptyExeption {
        if (projectRepository.findAll().isEmpty()) throw new ListIsEmptyExeption();
        return projectRepository.findAll();
    }

    @Transactional
    @Override
    public void removeAll(@Nullable final String userId) throws IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        projectRepository.deleteAllByUserId(userId);
    }

    @Transactional
    @Override
    public void removeOne(@Nullable final String projectId, @Nullable final String userId) throws IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        projectRepository.deleteOneByUserIdAndId(projectId, userId);
    }

    @Transactional
    @Override
    public void update(
            @Nullable final String userId, @Nullable final String projectId, @Nullable final String name,
            @Nullable final String description, @Nullable String status, @Nullable final String dateBegin, @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        switch (status) {
            case ("inprogress") :
                projectRepository.update(projectId, name, description, Status.INPROGRESS, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd));
                break;
            case ("planned") :
                projectRepository.update(projectId, name, description, Status.PLANNED, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd));
                break;
            case ("ready") :
                projectRepository.update(projectId, name, description, Status.READY, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd));
                break;
        }
    }

    @Override
    public void merge(
            @NotNull final Project project
    ) throws IncorrectValueException, ParseException {
        if (projectRepository.findByUserAndId(project.getId(), project.getUser().getId()) == null) {
            persist(project);
        } else {
            update(project.getUser().getId(), project.getId(), project.getName(), project.getDescription(), project.getStatus().displayName(),
                    dateFormat.format(project.getDateBegin()), dateFormat.format(project.getDateEnd()));
        }
    }

    @NotNull
    @Override
    public List<ProjectDto> searchByString(
            @Nullable final String userId, @Nullable final String string
    ) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (string == null || string.isEmpty()) throw new IncorrectValueException();
        if (projectRepository.getProjectsByString(userId, string).isEmpty()) throw new ListIsEmptyExeption();
        @Nullable final List<Project> projectList = projectRepository.getProjectsByString(userId, string);
        @Nullable final List<ProjectDto> projectDtoList = new ArrayList<>();
        for (Project project : projectList) {
            projectDtoList.add(convertEntityToDto(project));
        }
        return projectDtoList;
    }

    @Override
    public ProjectDto convertEntityToDto(Project project) {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setId(project.getId());
        projectDto.setName(project.getName());
        projectDto.setDescription(project.getDescription());
        projectDto.setStatus(project.getStatus());
        projectDto.setDateBegin(project.getDateBegin());
        projectDto.setDateEnd(project.getDateEnd());
        projectDto.setUserId(project.getUser().getId());
        return projectDto;
    }

    @Override
    public Project convertDtoToEntity(ProjectDto projectDto) {
        Project project = new Project();
        project.setId(projectDto.getId());
        project.setName(projectDto.getName());
        project.setDescription(projectDto.getDescription());
        project.setStatus(projectDto.getStatus());
        project.setDateBegin(projectDto.getDateBegin());
        project.setDateEnd(projectDto.getDateEnd());
        return project;
    }
}

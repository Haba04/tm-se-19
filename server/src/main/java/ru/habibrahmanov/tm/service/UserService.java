package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.habibrahmanov.tm.api.IUserService;
import ru.habibrahmanov.tm.repository.IUserRepository;
import ru.habibrahmanov.tm.dto.UserDto;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService extends AbstractService implements IUserService {

    @Autowired
    private IUserRepository userRepository;

    @Override
    public void registryAdmin(
            @Nullable final String login, @Nullable final String password, @Nullable final String passwordConfirm
    ) throws IllegalArgumentException, IncorrectValueException {
        if (login == null || login.isEmpty()) throw new IncorrectValueException();
        if (password == null || password.isEmpty()) throw new IncorrectValueException();
        if (!password.equals(passwordConfirm)) throw new IllegalArgumentException("PASSWORDS DO NOT MATCH");
        if (userRepository.findByLogin(login) == null) throw new IllegalArgumentException("SUCH USER EXISTS");
        userRepository.save(new User(login, HashUtil.md5(password)));
    }

    @Override
    public void registryUser(@Nullable final User user) throws IllegalArgumentException {
        if (user == null) throw new IllegalArgumentException();
        user.setPassword(HashUtil.md5(user.getPassword()));
        userRepository.save(user);

    }

    @Override
    public void updatePassword(
            @Nullable final String userId, @Nullable final String curPassword, @Nullable final String newPassword,
            @Nullable final String newPasswordConfirm) throws IncorrectValueException {
        @Nullable final User user = userRepository.getOne(userId);
        if (curPassword == null || curPassword.isEmpty()) throw new IncorrectValueException();
        if (newPassword == null || newPassword.isEmpty()) throw new IncorrectValueException();
        if (newPasswordConfirm == null || newPasswordConfirm.isEmpty()) return;
        if (!user.getPassword().equals(HashUtil.md5(curPassword))) {
            throw new IllegalArgumentException("CURRENT PASSWORD DOES NOT MATCH USER PASSWORD: " + user.getLogin());
        }
        if (!newPassword.equals(newPasswordConfirm)) throw new IllegalArgumentException("PASSWORDS DO NOT MATCH");
        user.setPassword(HashUtil.md5(newPassword));
    }

    @NotNull
    @Override
    public List<UserDto> findAll() throws ListIsEmptyExeption {
        if (userRepository.findAll().isEmpty()) throw new ListIsEmptyExeption();
        @Nullable final List<User> userList = userRepository.findAll();
        @Nullable final List<UserDto> userDtoList = new ArrayList<>();
        for (User user : userList) {
            userDtoList.add(convertEntityToDto(user));
        }
        return userDtoList;

    }

    @Override
    public void RemoveAll() {
        userRepository.deleteAll();
    }

    @NotNull
    @Override
    public UserDto findOne(@Nullable final String userId) throws IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final User user = userRepository.getOne(userId);
        @Nullable final UserDto userDto = convertEntityToDto(user);
        return userDto;
    }

    @NotNull
    @Override
    public UserDto findByLogin(@Nullable final String login) throws ListIsEmptyExeption {
        if (login == null || login.isEmpty()) throw new ListIsEmptyExeption();
        @Nullable final User user = userRepository.findByLogin(login);
        @Nullable final UserDto userDto = convertEntityToDto(user);
        return userDto;

    }

    @Override
    public UserDto convertEntityToDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setLogin(user.getLogin());
        userDto.setPassword(user.getPassword());
        userDto.setRole(user.getRole());
        return userDto;
    }

    @Override
    public User convertDtoToEntity(UserDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setLogin(userDto.getLogin());
        user.setPassword(userDto.getPassword());
        user.setRole(userDto.getRole());
        return user;
    }
}

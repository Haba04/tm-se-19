package ru.habibrahmanov.tm.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.enumeration.Status;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "app_task")
@NoArgsConstructor
@AllArgsConstructor
public final class Task extends AbstractEntity implements Comparable<Task>, Serializable {

    @Nullable
    @Column(name = "name")
    private String name;

    @Nullable
    @Column(name = "description")
    private String description;

    @Nullable
    @Column(name = "date_begin")
    private Date dateBegin;

    @Nullable
    @Column(name = "date_end")
    private Date dateEnd;

    @NotNull
    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    private Status status = Status.PLANNED;

    @Nullable
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    private Project project;

    @Nullable
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    public Task(@NotNull final String name, @NotNull final String description, @NotNull final Date dateBegin,
                @NotNull final Date dateEnd, @NotNull final Project project, @NotNull final User user) {
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
        this.project = project;
        this.user = user;
    }

    @Override
    public int compareTo(@NotNull Task o) {
        return this.getStatus().compareTo(o.getStatus());
    }
}

package ru.habibrahmanov.tm.bootstrap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.habibrahmanov.tm.endpoint.*;
import ru.habibrahmanov.tm.util.PublishUtil;
import java.util.*;

@Component
public final class Bootstrap {

    @Autowired
    private final Set<AbstractEndpoint> endpoits = new LinkedHashSet<>();

    public void init() {
        try {
            PublishUtil.publish(endpoits);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
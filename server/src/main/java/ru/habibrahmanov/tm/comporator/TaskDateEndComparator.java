package ru.habibrahmanov.tm.comporator;

import ru.habibrahmanov.tm.dto.TaskDto;
import java.util.Comparator;

public final class TaskDateEndComparator implements Comparator<TaskDto> {
    @Override
    public int compare(TaskDto o1, TaskDto o2) {
        return o1.getDateEnd().compareTo(o2.getDateEnd());
    }
}
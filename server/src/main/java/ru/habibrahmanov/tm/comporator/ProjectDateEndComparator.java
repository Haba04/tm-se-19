package ru.habibrahmanov.tm.comporator;

import ru.habibrahmanov.tm.entity.Project;
import java.util.Comparator;

public final class ProjectDateEndComparator implements Comparator<Project> {

    @Override
    public int compare(Project o1, Project o2) {
        return o1.getDateEnd().compareTo(o2.getDateEnd());
    }
}
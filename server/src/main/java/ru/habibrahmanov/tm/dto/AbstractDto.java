package ru.habibrahmanov.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@Getter
@Setter
public class AbstractDto {

    @NotNull
    @Id
    @Column(name = "id")
    private String id = UUID.randomUUID().toString();
}

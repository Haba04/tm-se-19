package ru.habibrahmanov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.enumeration.Status;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectDto extends AbstractDto implements Comparable<ProjectDto>, Serializable {
    @Nullable
    private String userId;
    @Nullable
    private String name;
    @Nullable
    private String description;
    @Nullable
    private Date dateBegin;
    @Nullable
    private Date dateEnd;
    @Nullable
    private Status status = Status.PLANNED;

    public ProjectDto(@NotNull final String userId, @NotNull final String name, @NotNull final String description,
                      @NotNull final Date dateBegin, @NotNull final Date dateEnd) {
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    @Override
    public int compareTo(@NotNull ProjectDto o) {
        return this.getStatus().compareTo(o.getStatus());
    }
}
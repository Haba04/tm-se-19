package ru.habibrahmanov.tm.exeption;

public class RemoveFailedException extends Exception {
    public RemoveFailedException() {
        super("REMOVE FAILED");
    }

    public RemoveFailedException(String message) {
        super(message);
    }

}

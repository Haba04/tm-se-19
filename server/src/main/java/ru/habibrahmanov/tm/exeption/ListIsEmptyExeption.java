package ru.habibrahmanov.tm.exeption;

public class ListIsEmptyExeption extends Exception {
    public ListIsEmptyExeption() {
        super("LIST IS EMPTY");
    }

    public ListIsEmptyExeption(String message) {
        super(message);
    }
}

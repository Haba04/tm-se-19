package ru.habibrahmanov.tm.exeption;

public class RoleTypeException extends Exception {
    public RoleTypeException() {
        super("ONLY ADMINISTRATOR CAN CALL THIS COMMAND");
    }
}

package ru.habibrahmanov.tm.wrapper;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.dto.UserDto;
import ru.habibrahmanov.tm.entity.User;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "userList")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserList {
    @Nullable
    @XmlElement(name = "user")
    private List<UserDto> userList = null;

    @Nullable
    public List<UserDto> getUserList() {
        return userList;
    }

    public void setUserList(List<UserDto> userList) {
        this.userList = userList;
    }
}

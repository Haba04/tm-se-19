package ru.habibrahmanov.tm.wrapper;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.dto.TaskDto;
import ru.habibrahmanov.tm.entity.Task;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "taskList")
@XmlAccessorType(XmlAccessType.FIELD)
public class TaskList {
    @Nullable
    @XmlElement(name = "task")
    private List<TaskDto> taskList = null;

    @Nullable
    public List<TaskDto> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<TaskDto> taskList) {
        this.taskList = taskList;
    }
}

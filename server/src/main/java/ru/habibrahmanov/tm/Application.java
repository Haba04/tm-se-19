package ru.habibrahmanov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.habibrahmanov.tm.bootstrap.Bootstrap;
import ru.habibrahmanov.tm.configuration.AppConfig;
import ru.habibrahmanov.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.DriverManager;

public class Application {
    public static void main(String[] args) throws Exception {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        @NotNull final Bootstrap bootstrap = context.getBean("bootstrap", Bootstrap.class);
        bootstrap.init();
    }
}
package ru.habibrahmanov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.habibrahmanov.tm.api.ISessionEndpoint;
import ru.habibrahmanov.tm.api.ISessionService;
import ru.habibrahmanov.tm.dto.SessionDto;
import ru.habibrahmanov.tm.entity.Session;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.SessionIsNotValidException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@NoArgsConstructor
@WebService(endpointInterface = "ru.habibrahmanov.tm.api.ISessionEndpoint")
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @Autowired
    private ISessionService sessionService;

    @Override
    @WebMethod
    public void validate(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws SessionIsNotValidException {
        sessionService.validate(session);
    }

    @Nullable
    @Override
    @WebMethod
    public SessionDto open(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) throws IncorrectValueException {
        return sessionService.open(login, password);
    }

    @Override
    @WebMethod
    public void close(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws SessionIsNotValidException {
        if (session == null) throw new SessionIsNotValidException();
        sessionService.close(session);
    }
}

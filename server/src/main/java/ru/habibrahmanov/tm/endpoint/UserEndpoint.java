package ru.habibrahmanov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.habibrahmanov.tm.api.ISessionService;
import ru.habibrahmanov.tm.api.IUserEndpoint;
import ru.habibrahmanov.tm.api.IUserService;
import ru.habibrahmanov.tm.dto.SessionDto;
import ru.habibrahmanov.tm.dto.UserDto;
import ru.habibrahmanov.tm.entity.Session;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.enumeration.Role;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RoleTypeException;
import ru.habibrahmanov.tm.exeption.SessionIsNotValidException;
import ru.habibrahmanov.tm.service.UserService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Component
@NoArgsConstructor
@WebService(endpointInterface = "ru.habibrahmanov.tm.api.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @Autowired
    private IUserService userService;

    @Autowired
    private ISessionService sessionService;

    @Override
    @WebMethod
    public void registryAdmin(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "passwordConfirm", partName = "passwordConfirm") @Nullable final String passwordConfirm
    ) throws IllegalArgumentException, UnsupportedEncodingException, NoSuchAlgorithmException, IncorrectValueException {
        userService.registryAdmin(login, password, passwordConfirm);
    }

    @Override
    @WebMethod
    public void registryUser(
            @WebParam(name = "user", partName = "user") @Nullable final User user
    ) throws IllegalArgumentException {
        userService.registryUser(user);
    }

    @Override
    @WebMethod
    public void updatePassword(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "curPassword", partName = "curPassword") @Nullable final String curPassword,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword,
            @WebParam(name = "newPasswordConfirm", partName = "newPasswordConfirm") @Nullable final String newPasswordConfirm
    ) throws UnsupportedEncodingException, NoSuchAlgorithmException, SessionIsNotValidException, IncorrectValueException {
        sessionService.validate(session);
        userService.updatePassword(session.getUserId(), curPassword, newPassword, newPasswordConfirm);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDto viewProfile(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws SessionIsNotValidException, IncorrectValueException {
        sessionService.validate(session);
        return userService.findOne(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public List<UserDto> findAll(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws ListIsEmptyExeption, SessionIsNotValidException, IncorrectValueException {
        sessionService.validate(session);
        if (userService.findOne(session.getUserId()).getRole().equals(Role.ADMIN))
            return userService.findAll();
        return null;
    }

    @Override
    @WebMethod
    public void RemoveAll(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws IncorrectValueException, RoleTypeException {
        if (userService.findOne(session.getUserId()).getRole().equals(Role.ADMIN))
            throw new RoleTypeException();
        userService.RemoveAll();
    }

    @Override
    @WebMethod
    public void editProfile(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "newLogin", partName = "newLogin") @Nullable final String newLogin
    ) throws SessionIsNotValidException, IncorrectValueException {
        sessionService.validate(session);
        userService.findOne(session.getUserId()).setLogin(newLogin);
    }
}

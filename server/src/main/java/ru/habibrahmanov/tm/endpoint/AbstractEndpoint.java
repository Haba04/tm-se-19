package ru.habibrahmanov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.habibrahmanov.tm.api.*;

@Component
public abstract class AbstractEndpoint {
    @Autowired
    protected ISessionService sessionService;
    @Autowired
    protected IUserService userService;
    @Autowired
    protected IUserEndpoint userEndpoint;
    @Autowired
    protected ITaskEndpoint taskEndpoint;
    @Autowired
    protected IProjectEndpoint projectEndpoint;
    @Autowired
    protected IProjectService projectService;
    @Autowired
    protected ITaskService taskService;
}

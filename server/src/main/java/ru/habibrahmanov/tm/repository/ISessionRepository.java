package ru.habibrahmanov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.habibrahmanov.tm.entity.Session;

@Repository
public interface ISessionRepository extends JpaRepository<Session, String> {
}

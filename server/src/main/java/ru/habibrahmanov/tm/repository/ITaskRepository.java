package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.enumeration.Status;

import java.util.Date;
import java.util.List;

@Repository
public interface ITaskRepository extends JpaRepository<Task, String> {

    @Query("SELECT task FROM Task task WHERE task.id = :id AND task.user.id = :userId")
    Task findOneByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Query("SELECT task FROM Task task WHERE task.user.id = :userId")
    List<Task> findAllByUserId(@NotNull @Param("userId") String userId);

    @Query("SELECT task FROM Task task WHERE task.user.id = :userId AND task.name LIKE :string OR task.name LIKE :string")
    List<Task> searchByString(@NotNull @Param("userId") String userId, @NotNull @Param("string") String string);

    @Modifying
    @Query("DELETE FROM Task WHERE id = :id AND user.id = :userId")
    void deleteOneByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id")  String id);

    @Modifying
    @Query("DELETE FROM Task WHERE project.id = :projectId AND user.id = :userId")
    void deleteAllByUserIdAndProjectId(@NotNull @Param("userId")  String userId, @NotNull @Param("projectId")  String projectId);

    @Modifying
    @Query("UPDATE Task SET name = :name, description = :description, dateBegin = :dateBegin, dateEnd = :dateEnd, status = :status WHERE id = :id AND user.id = :userId")
    void update(@Nullable @Param("userId") String userId, @Nullable @Param("id") String id, @Nullable @Param("name") String name, @Nullable @Param("description") String description,
                @Nullable @Param("status") Status status, @Nullable @Param("dateBegin") Date dateBegin, @Nullable @Param("dateEnd") Date dateEnd);
}

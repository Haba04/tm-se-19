package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.habibrahmanov.tm.entity.User;

@Repository
public interface IUserRepository extends JpaRepository<User, String> {

    @Query("SELECT user FROM User user WHERE user.login = :login")
    User findByLogin(@NotNull @Param("login") String login);
}

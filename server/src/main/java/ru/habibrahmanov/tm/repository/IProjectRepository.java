package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.enumeration.Status;
import java.util.Date;
import java.util.List;

@Repository
public interface IProjectRepository extends JpaRepository<Project, String > {

    @Query("SELECT project FROM Project project WHERE project.user.id = :userId AND project.name LIKE :string OR project.description LIKE :string")
    List<Project> getProjectsByString(@NotNull @Param("userId") String userId, @NotNull @Param("string") String string);

    @Query("SELECT project FROM Project project WHERE project.user.id = :userId AND project.id = :id")
    Project findByUserAndId(@Nullable @Param("userId") String userId, @Nullable @Param("id") String id);

    @Query("SELECT project FROM Project project WHERE project.user.id = :userId")
    List<Project> findAllByUserId(@NotNull @Param("userId") String userId);

    void deleteAllByUserId(@NotNull @Param("userId") String userId);

    @Modifying
    @Query("DELETE FROM Project WHERE user.id = :userId AND id = :id")
    void deleteOneByUserIdAndId(@NotNull @Param("id") String id, @NotNull @Param("userId") String userId);

    @Modifying
    @Query("UPDATE Project SET name = :name, description = :description, dateBegin = :dateBegin, dateEnd = :dateEnd, status = :status WHERE id = :projectId")
    void update(@Nullable @Param("projectId") String projectId, @Nullable @Param("name") String name, @Nullable @Param("description") String description,
                @Nullable @Param("status") Status status, @Nullable @Param("dateBegin") Date dateBegin, @Nullable @Param("dateEnd")  Date dateEnd);
}

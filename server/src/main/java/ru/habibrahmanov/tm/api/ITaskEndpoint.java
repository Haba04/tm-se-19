package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.dto.SessionDto;
import ru.habibrahmanov.tm.dto.TaskDto;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RemoveFailedException;
import ru.habibrahmanov.tm.exeption.SessionIsNotValidException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.text.ParseException;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    String URL = "http://localhost:8080/TaskEndpoint?wsdl";

    @WebMethod
    void persistTask(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session,
            @WebParam(name = "task", partName = "task") @Nullable Task task
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException;

    @WebMethod
    void insertTask(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description,
            @WebParam(name = "dateBegin", partName = "dateBegin") @Nullable String dateBegin,
            @WebParam(name = "dateEnd", partName = "dateEnd") @Nullable String dateEnd
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException;

    @Nullable
    @WebMethod
    TaskDto findOneTask(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String taskId
    ) throws IncorrectValueException, ListIsEmptyExeption, SessionIsNotValidException;

    @NotNull
    @WebMethod
    List<TaskDto> findAllTask(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session
    ) throws ListIsEmptyExeption, IncorrectValueException, SessionIsNotValidException;

    @NotNull
    @WebMethod
    List<TaskDto> findAllSortedByDateBeginTask(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session
    ) throws ListIsEmptyExeption, IncorrectValueException, SessionIsNotValidException;

    @NotNull
    @WebMethod
    List<TaskDto> findAllSortedByDateEndTask(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session
    ) throws ListIsEmptyExeption, IncorrectValueException, SessionIsNotValidException;

    @NotNull
    @WebMethod
    List<TaskDto> findAllSortedByStatusTask(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session
    ) throws IncorrectValueException, SessionIsNotValidException, ListIsEmptyExeption;

    @NotNull
    @WebMethod
    List<TaskDto> searchByStringTask(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session,
            @WebParam(name = "string", partName = "string") @Nullable String string
    ) throws IncorrectValueException, ListIsEmptyExeption, SessionIsNotValidException;

    @WebMethod
    void removeTask(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String taskId
    ) throws IncorrectValueException, RemoveFailedException, SessionIsNotValidException;

    @WebMethod
    void removeAllTask(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId
    ) throws IncorrectValueException, RemoveFailedException, SessionIsNotValidException;

    @WebMethod
    void updateTask(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session,
            @WebParam(name = "id", partName = "id") @Nullable String id,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description,
            @WebParam(name = "status", partName = "status") @Nullable String status,
            @WebParam(name = "dateBegin", partName = "dateBegin") @Nullable String dateBegin,
            @WebParam(name = "dateEnd", partName = "dateEnd") @Nullable String dateEnd
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException;

    @WebMethod
    void mergeTask(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session,
            @WebParam(name = "task", partName = "task") @Nullable Task task
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException;
}

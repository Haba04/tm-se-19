package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.dto.SessionDto;
import ru.habibrahmanov.tm.dto.UserDto;
import ru.habibrahmanov.tm.entity.Session;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RoleTypeException;
import ru.habibrahmanov.tm.exeption.SessionIsNotValidException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@WebService
public interface IUserEndpoint {

    String URL = "http://localhost:8080/UserEndpoint?wsdl";

    @WebMethod
    void registryAdmin(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "passwordConfirm", partName = "passwordConfirm") @Nullable String passwordConfirm
    ) throws IllegalArgumentException, UnsupportedEncodingException, NoSuchAlgorithmException, IncorrectValueException;

    @WebMethod
    void registryUser(
            @WebParam(name = "user", partName = "user") @Nullable User user
    ) throws IllegalArgumentException;

    @WebMethod
    void updatePassword(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session,
            @WebParam(name = "curPassword", partName = "curPassword") @Nullable String curPassword,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable String newPassword,
            @WebParam(name = "newPasswordConfirm", partName = "newPasswordConfirm") @Nullable String newPasswordConfirm
    ) throws UnsupportedEncodingException, NoSuchAlgorithmException, SessionIsNotValidException, IncorrectValueException;

    @Nullable
    @WebMethod
    UserDto viewProfile(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session
    ) throws SessionIsNotValidException, IncorrectValueException;

    @Nullable
    @WebMethod
    List<UserDto> findAll(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session
    ) throws ListIsEmptyExeption, SessionIsNotValidException, IncorrectValueException;

    @WebMethod
    void RemoveAll(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session
    ) throws IncorrectValueException, RoleTypeException;

    @WebMethod
    void editProfile(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session,
            @WebParam(name = "newLogin", partName = "newLogin") @Nullable String newLogin
    ) throws IncorrectValueException, SessionIsNotValidException;
}

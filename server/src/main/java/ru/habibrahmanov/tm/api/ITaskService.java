package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.dto.TaskDto;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RemoveFailedException;

import java.text.ParseException;
import java.util.List;

public interface ITaskService {
    void persist(@Nullable Task task) throws IncorrectValueException, ParseException;

    void insert(
            @Nullable String projectId, @Nullable String userId, @Nullable String name,
            @Nullable String description, @Nullable String dateBegin, @Nullable String dateEnd
    ) throws IncorrectValueException, ParseException;

    @NotNull
    TaskDto findOne(@Nullable String taskId, @NotNull String userId) throws IncorrectValueException, ListIsEmptyExeption;

    @NotNull
    List<TaskDto> findAll(@NotNull String userId) throws ListIsEmptyExeption, IncorrectValueException;

    @NotNull
    List<TaskDto> findAll() throws ListIsEmptyExeption;

    @NotNull
    List<TaskDto> findAllSortedByDateBegin(@NotNull String userId) throws ListIsEmptyExeption, IncorrectValueException;

    @NotNull
    List<TaskDto> findAllSortedByDateEnd(@NotNull String userId) throws ListIsEmptyExeption, IncorrectValueException;

    @NotNull
    List<TaskDto> findAllSortedByStatus(@Nullable String userId) throws IncorrectValueException, ListIsEmptyExeption;

    @NotNull
    List<TaskDto> searchByString (@Nullable String userId, @Nullable String string) throws IncorrectValueException, ListIsEmptyExeption;

    void remove(@Nullable String userId, @Nullable String taskId) throws IncorrectValueException, RemoveFailedException;

    void removeAll(@Nullable String userId, @Nullable String projectId) throws IncorrectValueException, RemoveFailedException;

    void update(@Nullable String projectId, @Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description,
                @Nullable String status, @Nullable String dateBegin, @Nullable String dateEnd
    ) throws IncorrectValueException, ParseException;

    void merge(
            @NotNull Task task
    ) throws IncorrectValueException, ParseException;

    TaskDto convertEntityToDto(Task task);

    Task convertDtoToEntity(TaskDto taskDto);
}

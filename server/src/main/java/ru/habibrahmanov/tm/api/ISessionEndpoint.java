package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.dto.SessionDto;
import ru.habibrahmanov.tm.entity.Session;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.SessionIsNotValidException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    String URL = "http://localhost:8080/SessionEndpoint?wsdl";

    @WebMethod
    void validate(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session
    ) throws SessionIsNotValidException;

    @Nullable
    @WebMethod
    SessionDto open(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password
    ) throws IncorrectValueException;

    @WebMethod
    void close(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session
    ) throws SessionIsNotValidException;
}

package ru.habibrahmanov.tm.util;

import java.sql.Date;

public class DateUtil {

    public static java.sql.Date utilToSqlDate(java.util.Date javaDate) {
        java.sql.Date sqlDate = null;
        if (javaDate != null) {
            sqlDate = new Date(javaDate.getTime());
        }
        return sqlDate;
    }

    public static java.util.Date sqlToUtilDate(java.sql.Date javaDate) {
        java.util.Date utilDate = null;
        if (javaDate != null) {
            utilDate = new Date(javaDate.getTime());
        }
        return utilDate;
    }
}
